//
//  MainVC.m
//  networkApp
//
//  Created by Artūrs Zikovs on 10/08/15.
//  Copyright (c) 2015 Recapture. All rights reserved.
//

#import "MainVC.h"
#import "ResultVC.h"

@interface MainVC ()

//IBOutlets
@property (weak, nonatomic) IBOutlet UITextField                *urlField;
@property (weak, nonatomic) IBOutlet UITextField                *connectionCountField;
@property (weak, nonatomic) IBOutlet UISegmentedControl         *cellularNetworkSwitch;
@property (weak, nonatomic) IBOutlet UISegmentedControl         *cachingSwitch;

@property (weak, nonatomic) IBOutlet UIButton                   *downloadButton;
@property (weak, nonatomic) IBOutlet UIActivityIndicatorView    *activityIndicator;

//Variables


@end

@implementation MainVC

#pragma mark - Initialization

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view, typically from a nib.
}

#pragma mark - View management

- (void)showResultsViewWithData:(NSArray *)resultsDataArray {
    
    //Updating visuals
    self.downloadButton.enabled = YES;
    self.activityIndicator.hidden = YES;

    //Presenting view with data
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    ResultVC *resultVC = [storyboard instantiateViewControllerWithIdentifier:@"ResultVC"];
    
    [resultVC setDataArray:resultsDataArray];
    [resultVC setModalPresentationStyle:UIModalPresentationFullScreen];
    [self presentViewController:resultVC animated:YES completion:nil];
}

#pragma mark - Button handling

- (IBAction)downloadButtonTapped:(id)sender {
    
    self.downloadButton.enabled = NO;
    self.activityIndicator.hidden = NO;
    
    //Preparing data
    BOOL useCellularData = [@(self.cellularNetworkSwitch.selectedSegmentIndex) boolValue];
    BOOL useCaching = [@(self.cachingSwitch.selectedSegmentIndex) boolValue];
    
    //Performing connection
    [self downloadDataWithURL:[NSURL URLWithString:self.urlField.text]
           useCellularNetwork:useCellularData
                   useCaching:useCaching
      withMaxConnectionsCount:[self.connectionCountField.text intValue]];
}

#pragma mark - Data download methods

- (void)downloadDataWithURL:(NSURL *)url
         useCellularNetwork:(BOOL)shouldUseCellularNetwork
                 useCaching:(BOOL)shouldUseCaching
    withMaxConnectionsCount:(NSInteger)maxConnectionsSize {
    
    //Preparing session configuration
    NSURLSessionConfiguration *sessionConfiguration = [NSURLSessionConfiguration defaultSessionConfiguration];
    sessionConfiguration.allowsCellularAccess = shouldUseCellularNetwork;
    
    sessionConfiguration.timeoutIntervalForRequest = 30;
    sessionConfiguration.timeoutIntervalForResource = 30;
    
    sessionConfiguration.HTTPMaximumConnectionsPerHost = maxConnectionsSize;
    
    sessionConfiguration.requestCachePolicy = (!shouldUseCaching) ? NSURLRequestReloadIgnoringLocalCacheData : NSURLRequestUseProtocolCachePolicy;
    
    //Setting up session
    NSURLSession *session = [NSURLSession sessionWithConfiguration:sessionConfiguration
                                                          delegate:nil
                                                     delegateQueue:[NSOperationQueue mainQueue]];
    
    //Creating network task
    NSURLSessionDataTask *dataTask = [session dataTaskWithURL:url
                                            completionHandler:^(NSData *responseData, NSURLResponse *response, NSError *error) {
                   
               if (error) {
                   //Handling error
                   NSString *errorString = [NSString stringWithFormat:@"Failed to download data due to error: %@", error.localizedDescription];
                   
                   UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Error"
                                                                   message:errorString
                                                                  delegate:nil
                                                         cancelButtonTitle:@"OK"
                                                         otherButtonTitles:nil];
                   [alert show];
               }
               else {
                   NSError *serializationError = nil;
                   
                   NSArray *serializedData = [NSJSONSerialization JSONObjectWithData:responseData options:0 error:&serializationError];
                   if (!error) {
                       
                       //Moving to main thread
                       dispatch_async(dispatch_get_main_queue(), ^{
                           
                           [self showResultsViewWithData:serializedData];
                       });
                       
                   }
                   else {
                       //Handling error
                       NSString *errorString = [NSString stringWithFormat:@"Failed to serialilize data with error: %@", serializationError.localizedDescription];
                       
                       UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Error"
                                                                       message:errorString
                                                                      delegate:nil
                                                             cancelButtonTitle:@"OK"
                                                             otherButtonTitles:nil];
                       [alert show];
                   }
               }
                   
                       }];
    
    [dataTask resume];
}


@end
