//
//  ResultVC.m
//  networkApp
//
//  Created by Artūrs Zikovs on 10/08/15.
//  Copyright (c) 2015 Recapture. All rights reserved.
//

#import "ResultVC.h"

#define kCellReuseIdentifier    @"resultCell"

@interface ResultVC ()

//IBOutlets
@property (weak, nonatomic) IBOutlet UITableView *resultTableView;


//Variables
@property (nonatomic) NSArray   *representedDataArray;

@end

@implementation ResultVC

#pragma mark - Initialization

- (void)setDataArray:(NSArray *)representedDataArray {

    //Setting up variables
    self.representedDataArray = representedDataArray;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
}

#pragma mark - Button handling

- (IBAction)backButtonTapped:(id)sender {
    
    [self dismissViewControllerAnimated:YES completion:nil];
}


#pragma mark - Table view delegate methods

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    
    return [self.representedDataArray count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:kCellReuseIdentifier
                                                                        forIndexPath:indexPath];
    
    cell.textLabel.text = [NSString stringWithFormat:@"%@", self.representedDataArray[indexPath.row]];
    cell.textLabel.numberOfLines = 0;

    
    return cell;
}

@end
