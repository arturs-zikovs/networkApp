//
//  ResultVC.h
//  networkApp
//
//  Created by Artūrs Zikovs on 10/08/15.
//  Copyright (c) 2015 Recapture. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ResultVC : UIViewController

#pragma mark - Initialization
- (void)setDataArray:(NSArray *)representedDataArray;

@end
